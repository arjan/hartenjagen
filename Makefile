all: prepare deploy

prepare:
	ansible-playbook -i prod prepare.yml

deploy: html
	ansible-playbook -i prod deploy.yml

html:
	cd web/ && brunch build
#	cd web/ && brunch build --production
