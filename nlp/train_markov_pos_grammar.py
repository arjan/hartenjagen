#!/usr/bin/env python

import os
import glob
import nltk
import cPickle

from collections import defaultdict

N = 3

alpinoTagger = nltk.data.load('taggers/alpino_brill_aubt.pickle')


files = glob.glob("corpus/*.txt")
for f in files:
    dat = f.replace(".txt", ".dat")
    if os.path.exists(dat):
        continue

    print f
    groups = defaultdict(float)
    words = nltk.word_tokenize(open(f).read())
    tags = [t[1] for t in alpinoTagger.tag(words)]
    #f = 1/float(len(tags)-N)
    for i in xrange(len(tags)-N):
        pair = tuple(tags[i:i+N])
        groups[pair] += 1
    cPickle.dump(groups, open(dat, "w"))

groups = defaultdict(float)
files = glob.glob("corpus/*.dat")
for f in files:
    g = cPickle.load(open(f, "r"))
    for pair, score in g.iteritems():
        groups[pair] += score

total = sum(groups.values())
print total
groups = dict([(pair, s/total) for pair, s in groups.iteritems()])
print groups
cPickle.dump(groups, open("markov_pairs_{}.dat".format(N), "w"))
print("Done!")
