#!/usr/bin/env python

import re
import os
import glob
import nltk
import cPickle

from collections import defaultdict

N = 3

files = glob.glob("corpus/*.txt")
for f in files:
    dat = f.replace(".txt", ".voc")
    if os.path.exists(dat):
        continue

    print f
    groups = defaultdict(float)
    text = re.sub("[^a-z]+", " ", open(f).read().lower()).strip()
    tags = nltk.word_tokenize(text)
    for i in xrange(len(tags)-N):
        pair = tuple(tags[i:i+N])
        groups[pair] += 1
    #print groups
    cPickle.dump(groups, open(dat, "w"))

groups = defaultdict(float)
files = glob.glob("corpus/*.voc")
for f in files:
    print f
    g = cPickle.load(open(f, "r"))
    for pair, score in g.iteritems():
        groups[pair] += score

total = sum(groups.values())
print total
groups = dict([(pair, s/total) for pair, s in groups.iteritems()])
#print groups
cPickle.dump(groups, open("markov_pairs_voc_{}.dat".format(N), "w"))
print("Done!")
