// Declare app level module which depends on filters, and services
angular.module('app', ['templates', 'ngSanitize', 'ngResource', 'ui.router'])
  .constant('VERSION', '0.7.0')
  .config(function appConfig($stateProvider, $locationProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise("/game/read");

    var resolve = {profile: ['Backend', '$state', '$rootScope', function(Backend, $state, $rootScope) {
      return Backend.getProfile().then(function(profile) {
        ga('send', 'event', 'load-profile', profile.email);
        $rootScope.profile = profile;
        return profile;
      }, function() {
        $state.go('login');
      });
    }]};
    
	$stateProvider
      .state('login', {
	    url: "/login", // root route
	    views: {
		  "mainView": {
		    templateUrl: "app/views/login.html",
		    controller: 'LoginCtrl'
		  }
	    }
	  })

      .state('signup', {
	    url: "/signup", // root route
	    views: {
		  "mainView": {
		    templateUrl: "app/views/signup.html",
		    controller: 'SignupCtrl'
		  }
	    }
	  })

      .state('game', {
	    url: "/game", // root route
        resolve: resolve, 
	    views: {
		  "mainView": {
		    templateUrl: "app/views/game.html",
		    controller: 'GameCtrl'
		  }
	    }
	  })

      .state('game.write', {
        url: '/write'
      })

      .state('game.read', {
        url: '/read'
      })
    
    ;

  })

;


