angular.module('app')

  .controller('LoginCtrl', function LoginCtrl($scope, $state, Backend) {

    // check if not logged in
    Backend.getProfile().then(function() { $state.go('game.read'); });
    
    $scope.data = {
      email: "", password: ""
    };
    
    $scope.login = function() {

      if (!$scope.loginForm.$valid) {
        $scope.error = "Vul a.u.b. alle velden correct in.";
        return;
      }
      
      Backend.login($scope.data.email, $scope.data.password).then(
        function(result) {
          ga('send', 'event', 'login', $scope.data.email);
          $state.go('game.read');
        },
        function(error) {
          console.error(error);
          $scope.error = "Onbekende account... Nieuwe gebruiker? Registreer eerst!";
        }
      );

    };
    
  })


  .controller('ForgotPWCtrl', function ForgotPWCtrl($scope) {
  })

  .controller('SignupCtrl', function SignupCtrl($scope, $state, Backend) {

    // check if not logged in
    Backend.getProfile().then(function() { $state.go('game.read'); });
    
    $scope.signup = function() {

      if (!$scope.signupForm.$valid) {
        $scope.error = "Vul a.u.b. alle velden correct in.";
        return;
      }

      Backend.signup($scope.data).then(
        function(result) {
          ga('send', 'event', 'signup', $scope.data.email);
          $state.go('game.read');
        },
        function(error) {
          console.error(error);
          $scope.error = "Er ging iets fout, probeert U het overnieuw, misschien met een ander e-mailadres?";
        }
      );

    };
    
  })

;
