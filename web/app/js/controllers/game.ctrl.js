angular.module('app')

  .controller('GameCtrl', function GameCtrl($scope, $state, Backend, profile) {

    $scope.profile = profile;
    
    function setGamestate(st) {
      $scope.gameState = st.name.replace('game.', '');
      $scope.$broadcast('gameState', $scope.gameState);
    }

    setGamestate($state.$current);
    $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){ 
      setGamestate(toState);
    });

    $scope.selectLetter = function(l) {
      $scope.currentLetter = l;
    };

    $scope.clickRead = function() {
      ga('send', 'event', 'nav-button', 'read');
      $state.go('game.read');
    };

    $scope.clickWrite = function() {
      ga('send', 'event', 'nav-button', 'write');
      $state.go('game.write');
    };
    
    function reloadLetters() {
      return Backend.getLetters().then(function(letters) {
        $scope.hasDraft = $scope.profile.current_state.text && ($scope.profile.current_state.text.length > 0);
        $scope.letters = letters.letters;
        $scope.letters.reverse(); 
        $scope.$broadcast('gotLetters');
      });
    }
    reloadLetters();

    $scope.$on('textChange', (s, saved) => {
      ga('send', 'event', 'letter', 'parse');
      Backend.parse(saved.text, saved.cursor).then((r) => {
        $scope.$emit('stateUpdate', r);
      });
    });

    $scope.$on('letterSent', (s, saved) => {
      // reload profile
      Backend.getProfile().then(p => { $scope.profile = p; });
      reloadLetters().then(() => $state.go('game.read'));
    });

    $scope.letterImage = (l) => {
      return '/images/violet/' + l.text.replace(/<.*?>/g, '');
    };
  })
;


