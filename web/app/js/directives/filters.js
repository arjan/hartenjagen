angular.module('app')

  .filter('date', function() {
    return function(d) {
      return moment(d).format('LL');
    };
  })

  .filter('summarize', function() {
    return function(s) {
      var n = 30;
      return s.length > n ? s.substr(0, n)+"..." : s;
    };
  })
;
