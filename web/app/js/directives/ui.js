angular.module('app')

  .directive('gameProgressBox', function gameProgressBox() {
	return {
	  restrict: 'E',
	  templateUrl: 'app/views/ui/game_progress_box.html',
	  link: function link($scope, $element, $attrs) {
        var baseScore = $scope.profile.score || 0;
        $scope.score = baseScore;
        
        $scope.scoreRange = _.range(0,100);

        $scope.letter_count = $scope.profile.stats.letter_count;
        $scope.word_count = $scope.profile.stats.word_count;

        $scope.$on('stateUpdate', function(src, newState) {
          $scope.score = baseScore + newState.text_score;
        });

	  }
      
    };
  })

  .directive('userDetailsBox', function userDetailsBox(Backend, $state) {
	return {
	  restrict: 'E',
	  templateUrl: 'app/views/ui/user_details_box.html',
	  link: function link($scope, $element, $attrs) {

        $scope.logout = function() {
          Backend.logout().then(function() {
            $state.go('login');
          });
        };
        
      }
    };
  })

  .directive('writeControlBar', function writeControlBar($state, Backend) {
	return {
	  restrict: 'E',
	  templateUrl: 'app/views/ui/write_control_bar.html',
	  link: function link($scope, $element, $attrs) {

        $scope.$on('stateUpdate', function(src, newState) {
          $scope.state = newState;
        });
        
        $scope.send = function() {
          Backend.send({text: $scope.state.text}).then(function(r) {
            ga('send', 'event', 'letter', 'sent');
            $scope.$emit('letterSent');
          });
        };
        
	  }
    };
  })

  .directive('letterControlBar', function writeControlBar($rootScope) {
	return {
	  restrict: 'E',
	  templateUrl: 'app/views/ui/letter_control_bar.html',
	  link: function link($scope, $element, $attrs) {
        $scope.next = () => {
          ga('send', 'event', 'letter-control-bar', 'next');
          $scope.$parent.$broadcast('next');
        };
        $scope.prev = () => {
          ga('send', 'event', 'letter-control-bar', 'prev');
          $scope.$parent.$broadcast('prev');
        };
	  }
    };
  })

  .directive('letterStack', function writeControlBar($state, $timeout) {
	return {
	  restrict: 'A',
      link: function(scope, el) {
        var stack = $(el);
        stack.addClass('letter-stack');

        var rnd = function(el) {
          var variance = Math.min(Math.max((1 - (el.height() / 4000)), 0), 1);
          var maxangle = 180;
          var d = Math.floor(maxangle * (variance * (Math.random()-0.5)*2))/(maxangle/2);
          el.css('transform', 'rotate(' + d + 'deg) translate(0,0)');
        };

        var randomize = () => {
          $('div.letter-stack > .box').each(function() { rnd($(this)); });
        };
        scope.$on('gotLetters',  () => $timeout(randomize, 0));
        
        scope.$on('next',  () => {
          //var stack = $('div.stack');
          var stackEl = stack.find(":eq(0)");
          stackEl.css('transform', '');

          stack.toggleClass("next", true);
          setTimeout(() => {
            stack.toggleClass("skipanim", true);
            stack.toggleClass("next", false);
            stack.toggleClass("next-end", true);
            // shift
            stack.append(stackEl);
            stack.toggleClass("skipanim", false);

            // close
            setTimeout(() => {
              stack.toggleClass("next-end", false);
              rnd(stackEl);
            }, 50);
          }, 400);
        });

        scope.$on('prev',  () => {
          var stackEl = stack.children().last();
          stackEl.css('transform', '');

          stack.toggleClass("prev", true);
          setTimeout(() => {
            stack.toggleClass("skipanim", true);
            stack.toggleClass("prev", false);
            stack.toggleClass("prev-end", true);
            // shift
            stack.toggleClass("skipanim", false);
            stack.prepend(stackEl);

            // close
            setTimeout(() => {
              stack.toggleClass("prev-end", false);
              rnd(stackEl);
            }, 50);
          }, 400);
        });
        
      }
    };
  })


  .directive('letterEditor', function(Backend, $timeout) {
    return {
      restrict: 'E',
      replace: true,
      template: '<div class="letter-editor"><div class="editable" autofocus></div></div>',
      link: function(scope, elem) {

        var overlay = $("<div class='overlay'></div>").prependTo(elem);
        var editable = $(elem).find(".editable");

        elem.on('click', () => $(editable).focus());

        scope.$on('gameState', (s, state) => {
          editable[0].contentEditable = (state == 'write');
        });

        function saveText() {
          var CURSOR = "###CURSOR###";
          var html = editable.html();
          var text = html.replace(/(<br>)*$/, '').replace(/<br>/g, "\n").replace(/<.*?>/g, '').replace(/&nbsp;/g, ' ');

          var cursor = text.indexOf(CURSOR);

          text = text.replace(CURSOR, "");
          return {
            text: text,
            cursor: cursor
          };
        }

        editable.on('keydown', function(e) {
          if (e.keyCode === 13) {
            e.preventDefault();
            overlay.hide();
            document.execCommand('insertHTML', false, '<br><br>');
            return false;
          }
          if (e.keyCode === 8) {
            // backspace
            overlay.hide();
          }

          return true;
        });


        var oldHtml = false;

        function checkPastePreventionInterval() {
          $timeout(checkPastePreventionInterval, 500);
          var check = editable.html();
          if (oldHtml === false) {
            oldHtml = check;
            return;
          }

          var delta = check.length - oldHtml.length;
          if (delta > 30) {
            // prevent copypaste
            ga('send', 'event', 'letter-editor', 'copypaste-prevented');
            editable.html(oldHtml);
          } else {
            oldHtml = check;
          }

        }
        $timeout(checkPastePreventionInterval, 500);
        
        var saved;
        var prev = '';
        var parseText = _.debounce(function() {
          var check = editable.html();
          if (check == prev) return;
          prev = check;

          saved = saveText();
          scope.$emit('textChange', saved);
        }, 400);
        editable.on('keyup blur', parseText);

        if (scope.profile.current_state && scope.profile.current_state.text) {
          editable.html(scope.profile.current_state.text.replace(/\n/g, "<br>"));
          parseText();
        }
        $(editable).focus();

        scope.$on('stateUpdate', function(src, newState) {
          var html = newState.html.replace(/\n/g, "<br>");
          overlay.html(html);
          overlay.show();
        });
      }
    };
  })


;
