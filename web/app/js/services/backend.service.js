angular.module('app')
  .factory('Backend', function Backend($http) {

    function doPost(path, data) {
        return $http.post('/api/' + path, data).then(function(r) {
          return r.data;
        });
    };

    function jsonGetter(path) {
      return function() {
        return $http.get('/api/' + path).then(function(r) {
          return r.data;
        });
      };
    }

    function jsonPoster(path) {
      return function(data) {
        return doPost(path, data);
      };
    }
    
    return {
      getProfile: jsonGetter('profile'),
      login: function(email, password) {
        return doPost('login', {email: email, password: password});
      },
      signup: jsonPoster('signup'),
      logout: jsonPoster('logout'),
      saveState: jsonPoster('game/state'),
      send: jsonPoster('send'),
      parse: function(text, cursor) {
        return doPost('draft', {text: text, cursor: cursor});
      },
      getLetters: jsonGetter('letters')
    };
  })
;
