var path = require('path');

exports.config = {
  // See docs at https://github.com/brunch/brunch/blob/master/docs/config.md
  modules: {
	definition: false,
	wrapper: false
  },

  paths: {
	"public": 'public',
	"watched": ['app', 'vendor']
  },

  files: {
	templates: {
	  joinTo: {
		'js/templates.js': /^app\/views/,
      }
    },
	javascripts: {
	  joinTo: {
		'js/app.js': /^app/,
        'js/vendor.js': /^(bower_components|vendor)/
	  },
	  order: {
		before: [
		  // jquery
		  'bower_components/jquery/jquery.js',

		  // angular
		  'bower_components/angular/angular.js',

		  // bootstrap
		  'bower_components/bootstrap/dist/js/bootstrap.js'
		]
	  }
	},
	stylesheets: {
	  joinTo: {
		'css/app.css': /^app/
	  }
	}
  },

  plugins: {
	ng_annotate: {
	  pattern: /^app/
	},
	traceur: {
	  paths: /^app/,
	  options: {
		experimental: true
	  }
	},
	autoprefixer: {
	  browsers: [
		"last 2 version",
		"> 1%", // browsers with > 1% usage
		"ie >= 9"
	  ],
	  cascade: false
	}
  },

  server: {
	port: 3333
  },

  conventions: {
	assets: /app(\\|\/)assets(\\|\/)/
  },

  sourceMaps: true
};
