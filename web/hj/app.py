import os
from flask import Flask, request, send_from_directory
import flask_restful 
from flask_mail import Mail

# flask-peewee database, but could be SQLAlchemy instead.
from flask_peewee.db import Database

# set the project root directory as the static folder, you can set others.
app = Flask(__name__, static_url_path='', static_folder=None)

app.config.from_object('hj.config.Config')
app.secret_key = "32492347r9832r790es8fs9df"

db = Database(app)

public_dir = os.path.abspath(os.path.dirname(__file__) + '/../public')

@app.route('/')
def send_index():
    return send_from_directory(public_dir, 'index.html')
    
@app.route('/<path:path>')
def send_static(path):
    return send_from_directory(public_dir, path)


api = flask_restful.Api(app)

mail = Mail(app)
