"""
auth imports app and models, but none of those import auth
so we're OK
"""
from flask_peewee.auth import Auth  # Login/logout views, etc.
from flask import session, abort

from app import app, db
from models import User

from functools import wraps


auth = Auth(app, db, user_model=User)


def login_required(f):
    @wraps(f)
    def inner(*args, **kwargs):
        if not session.get('user'):
            abort(401)
        return f(*args, **kwargs)
    return inner
