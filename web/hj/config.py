import os


class Config(object):
    #DEBUG = True
    DATABASE = {
        'name': os.environ.get('DBNAME', 'app.db'),
        'engine': 'peewee.SqliteDatabase'
    }
    LETTER_MAIL_SENDER = ("Violet", "violet@hartenjagen.nl")
    DEFAULT_MAIL_SENDER = ("Datingsite Hartenjager", "info@hartenjagen.nl")
    BASE_URL = 'http://hartenjager.nl/'

    GAME_MIN_WAIT = 20
    GAME_RANDOM_WAIT = 30

    MAX_LETTERS = 7
