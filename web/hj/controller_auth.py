from wtforms import Form, BooleanField, TextField, PasswordField, validators
from flask import request, abort, jsonify, session
from flask_restful import reqparse
from peewee import IntegrityError
from datetime import datetime

from app import app
from models import User
from auth import login_required


@app.route("/api/signup", methods=['POST'])
def signup():
    parser = reqparse.RequestParser()
    parser.add_argument('name', required=True, type=str, help='Name')
    parser.add_argument('country', required=False, type=str, help='Country')

    parser.add_argument('email', required=True, type=str, help='E-mail address')
    parser.add_argument('password', required=True, type=str, help='Password')
    args = parser.parse_args()
    try:
        user = User.signup(**args)
    except IntegrityError, e:
        print e
        abort(400)
    session['user'] = user.id
    return jsonify(user_id=user.id)


@app.route("/api/login", methods=['POST'])
def login():
    parser = reqparse.RequestParser()
    parser.add_argument('email', required=True, type=str, help='E-mail address')
    parser.add_argument('password', required=True, type=str, help='String')
    args = parser.parse_args()

    try:
        user = User.get(email=args['email'], password=args['password'])
    except User.DoesNotExist:
        abort(401)
    session['user'] = user.id
    return jsonify(**user.data())


@app.route("/api/profile", methods=['GET'])
@login_required
def profile():
    user = User.get(id=session['user'])
    return jsonify(**user.data())


@app.route("/api/game/state", methods=['POST'])
@login_required
def post_current_state():
    user = User.get(id=session['user'])
    user.current_state = request.get_json()
    user.save()
    return jsonify(**user.current_state)


@app.route("/api/logout", methods=['POST'])
@login_required
def logout():
    del session['user']
    return "OK"
