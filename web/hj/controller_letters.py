from flask import request, jsonify, session, abort
import random
from datetime import datetime

import parser
from app import app
from models import User, Letter
from auth import login_required

@app.route("/api/draft", methods=['POST'])
@login_required
def draft():
    user = User.get(id=session['user'])
    user.current_state = parser.from_request(request)
    user.save()
    return jsonify(**user.current_state)


@app.route("/api/letters", methods=['GET'])
@login_required
def letters():
    user = User.get(id=session['user'])
    letters = [Letter.violet(user).data()] + [l.data() for l in user.letters]
    return jsonify(letters=letters)


@app.route("/api/send", methods=['POST'])
@login_required
def send():
    user = User.get(id=session['user'])
    if user.status != 'writing':
        abort(400)

    parsed = parser.from_request(request)
    if not parsed['acceptable']:
        abort(400)

    text = request.get_json()['text']

    letter = user.sendLetter(text, parsed)
    return jsonify(**letter.data())



@app.route("/api/receive", methods=['POST'])
@login_required
def receive():
    user = User.get(id=session['user'])
    if user.status != 'waiting':
        abort(400)

    letter = user.receiveLetter()
    return jsonify(**letter.data())
    