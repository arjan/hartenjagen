from hj.models import User
from hj.app import app
from datetime import datetime


def main():
    users = list(User.select().where(User.status == 'waiting', User.wait_until < datetime.now()))
    for u in users:
        u.receiveLetter()
        print "Sent letter to %s" % (u.name)

if __name__ == "__main__":
    with app.app_context():
        main()
