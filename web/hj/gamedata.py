import json
import os
import random

def contains_sublist(lst, sublst):
    n = len(sublst)
    return any((sublst == lst[i:i+n]) for i in xrange(len(lst)-n+1))


class GameData(object):

    def __init__(self):
        f = os.path.join(os.path.dirname(__file__), "gamedata.json")
        self.data = json.loads(open(f).read())

    def getTextMatches(self, input):
        """ return dictionary of subject id -> [list of matches] """
        lemmas = []
        result = {}
        for sid, subject in self.data["subjects"].iteritems():
            m, l = self.getSubjectTextMatches(input, subject["triggers"])
            if len(m) > 0:
                result[sid] = m
                lemmas = lemmas + l

        # calculate current text score
        score = self.calculateScore([result])
        return result, score, lemmas

    def getSubjectTextMatches(self, input, triggers):
        result = []
        lemmas = []
        for tid, trigger in triggers.iteritems():
            if type(trigger["match"]) != list:
                trigger["match"] = [trigger["match"]]
            for m in trigger["match"]:
                l = self.getTriggerMatch(m, input)
                if len(l) > 0:
                    result.append(tid)
                    lemmas.append(l)
                    break
        return result, lemmas

    def getTriggerMatch(self, trigger, lemmas):
        if isinstance(trigger, basestring) and trigger in lemmas:
            return [trigger]
        if isinstance(trigger, list) and contains_sublist(lemmas, trigger):
            return trigger
        return []

    def calculateScore(self, matches):
        score = 0
        triggers = {}
        for result in matches:
            for sid, trigger_ids in result.iteritems():
                if sid not in triggers:
                    triggers[sid] = set()
                triggers[sid] |= set(trigger_ids)
        for sid, trigger_ids in triggers.iteritems():
            for tid in trigger_ids:
                score += self.data["subjects"][sid]["triggers"][tid]["score"]
        return score

    def getTouchedSubjects(self, matches):
        r = {}
        for m in matches:
            for sid, trigger_ids in m.iteritems():
                r[sid] = len(trigger_ids) / float(len(self.data["subjects"][sid]["triggers"].keys()))
        return r

    def generateLetter(self, user):
        """ The letter generator. """
        # find out which subjects the user touched
        subjects_all, subjects_last = user.getTouchedSubjects()

        prev_sentences = user.getPrevSentences()
        # hints
        hints = self.generateHints(subjects_all)

        # responses to subjects which are not 100% complete yet
        responses = self.generateResponses(subjects_all)

        # "change the subject" responses for subjects which are 100% complete
        newsubjects = self.generateNewSubjects(subjects_all)

        # filter all based on what has been generated earlier
        sentences = list(set(hints + responses + newsubjects) - prev_sentences)

        random.shuffle(sentences)
        # pick max 3
        picked = sentences[:3]

        stats = {'seen': subjects_all, 'picked': picked, 'sentences': []}

        if picked == []:
            return (False, stats) # final letter indicator

        def pick_score_sentence(l):
            idx = int(len(l) * score/100.0)
            n = 0
            while True:
                if n > 3:
                    return l[idx] # fallback
                if (idx+n < len(l)) and l[idx+n] not in prev_sentences:
                    return l[idx+n]
                if (idx-n >= 0 and idx-n < len(l)) and l[idx-n] not in prev_sentences:
                    return l[idx-n]
                n += 1
                    

        l = self.data["letter"]
        score = user.getScore()

        sentences = [pick_score_sentence(l["openings"])] + picked + [pick_score_sentence(l["closings"])]
        text = "\n".join([self.data["sentences"][k] for k in sentences])
        stats['sentences'] = sentences
        print "sentences:", sentences
        return (text, stats)

    def generateHints(self, subjects_all):
        """ Get subject-specific hint sentences for subjects that have not been mentioned yet"""
        allsubjects = set(self.data["subjects"].keys())
        # remove the seen ones
        subjects = list(allsubjects - set(subjects_all.keys()))
        # get sentence ids
        sentences = [self.data["subjects"][s]["hint"] for s in subjects if "hint" in self.data["subjects"][s]]
        return sentences

    def generateResponses(self, subjects_all):
        """ Generate responses to subjects which are not 100% complete yet """
        subjects = [s for s,v in subjects_all.iteritems() if v < 1]
        sentences = [random.choice(self.data["subjects"][s]["sentences"]) for s in subjects]
        return sentences

    def generateNewSubjects(self, subjects_all):
        """ Generate "change the subject" sentences for subjects which are 100% complete """
        subjects = [s for s,v in subjects_all.iteritems() if v >= 1]
        sentences = [self.data["subjects"][s]["newsubject"] for s in subjects if "newsubject" in self.data["subjects"][s]]
        if len(sentences) == 0 and len(subjects) > 0:
            # no specific hints available; take a generic one
            sentences = [random.choice(self.data["letter"]["newsubject"])]
        return sentences

    def getMedia(self):
        return random.choice(self.data["media"])

    def generateFinalLetter(self, user):
        score = user.getScore()
        if score >= 100:
            kind = "final-winner"
        else:
            kind = "final-loser"
        text = self.data["sentences"][self.data["letter"][kind]]
        return (text, {'kind': kind, 'score': score})


game = GameData()
