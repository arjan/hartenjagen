import subprocess

from flask import request

import parser
import models
import controller_auth
import controller_letters

from app import app, api

api.init_app(app)
models.create_tables()

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
