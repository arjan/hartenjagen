"""
models imports app, but app does not import models so we haven't created
any loops.
"""
import datetime
import json
import os
import random
from datetime import datetime, timedelta

from flask_mail import Message
from flask_peewee.auth import BaseUser  # provides password helpers..
from peewee import *

from app import db, app, mail
from gamedata import game

class JSONField(TextField):
    def db_value(self, value):
        return json.dumps(value)

    def python_value(self, value):
        if isinstance(value, basestring):
            return json.loads(value)
        return value


class User(db.Model, BaseUser):
    name = CharField()
    email = CharField(unique=True)
    password = CharField()
    created = DateTimeField(null=False)
    status = CharField(null=False)
    wait_until = DateTimeField(null=True)

    stats = JSONField(null=True)
    current_state = JSONField(null=True)

    def __unicode__(self):
        return self.email

    @classmethod
    def signup(cls, **args):
        stats = {'word_count': 0, 'letter_count': 0}
        u = User.create(created=datetime.now(), status='writing', current_state={}, stats=stats, **args)
        u._sendWelcomeMail()
        return u


    def data(self):
        d = self._data.copy()
        del d['password']
        d['letters'] = [s.data() for s in self.letters]
        d['score'] = self.getScore()
        return d

    def getScore(self):
        return game.calculateScore([l.stats['text_matches'] for l in self.letters if l.kind == 'sent'])

    def getCurrentDraft(self):
        return self.current_state.get('text', None)

    def sendLetter(self, text, parsed):
        letter = Letter.createSent(text, self, parsed)
        self.status = 'waiting'
        d = app.config['GAME_MIN_WAIT'] + int(random.random() * app.config['GAME_RANDOM_WAIT'])
        self.wait_until = datetime.now() + timedelta(seconds=d)
        self.current_state = {'text': ''}
        self.stats['word_count'] += letter.stats['word_count']
        self.stats['letter_count'] += 1
        self.save()
        return letter

    def receiveLetter(self):

        (text, stats) = game.generateLetter(self)
        
        if self.stats['letter_count'] >= app.config['MAX_LETTERS'] or text is False:
            self.status = 'done'
            (text, stats) = game.generateFinalLetter(self)
            letter = Letter.createReceived(text, self, stats)

        else:
            self.status = 'writing'
            # generate a letter
            letter = Letter.createReceived(text, self, stats)
            if random.random() > 0.4:
                letter = Letter.media(self)

        self.save()
        # send mail
        self._sendReceivedMail()
        return letter

    def _sendReceivedMail(self):
        msg = Message('Je hebt een brief gekregen', sender=app.config['LETTER_MAIL_SENDER'])
        msg.body = "Hoi %s,\n\nIk heb je een brief gestuurd! Ga snel naar de website om hem te lezen:\n%s\n\nGroetjes, Violet" % (self.name, app.config['BASE_URL'])
        msg.add_recipient(self.email)
        mail.send(msg)

    def _sendWelcomeMail(self):
        msg = Message('Welkom!', sender=app.config['DEFAULT_MAIL_SENDER'])
        msg.body = "Hoi %s,\n\nJe hebt je ingeschreven op onze datingsite Hartenjager. Voorlopig hebben we alleen Violet wiens hart je kunt veroveren. We hopen spoedig op meer inschrijvingen.\n\nWanneer je een brief hebt verstuurd, krijg je een email als er weer een antwoordbrief voor je klaar ligt.\n\nSucces!" % (self.name)
        msg.add_recipient(self.email)
        mail.send(msg)

    def getTouchedSubjects(self):
        sent = [l for l in self.letters if l.kind == 'sent']
        all = game.getTouchedSubjects([l.stats['text_matches'] for l in sent])
        last = game.getTouchedSubjects([sent[-1].stats['text_matches']])
        return all, last

    def getPrevSentences(self):
        recv = [l for l in self.letters if l.kind == 'received']
        return reduce(lambda s, l: s | set(l.stats.get('sentences', [])), recv, set())

class Letter(db.Model):
    user = ForeignKeyField(User, related_name='letters')
    kind = CharField()
    created = DateTimeField()

    text = TextField()
    stats = JSONField(null=True)

    def data(self):
        return dict(self._data.copy(), html=self._html())

    def _html(self):
        return "<p>" + ("</p><p>".join(self.text.split("\n"))) + "</p>"

    @classmethod
    def createSent(cls, text, user, parsed):
        letter = Letter.create(text=text, user=user, kind='sent', created=datetime.now(), stats=parsed)
        letter.save()
        return letter

    @classmethod
    def createReceived(cls, text, user, stats):
        letter = Letter.create(text=text, user=user, kind='received', created=datetime.now(), stats=stats)
        letter.save()
        return letter
        
    @classmethod
    def violet(cls, user):
        letter = Letter()
        letter.created = user.created
        letter.text = open(os.path.join(os.path.dirname(__file__), 'violet.txt')).read()
        letter.kind = 'received'
        return letter

    @classmethod
    def media(cls, user):
        letter = Letter(user=user, kind='media', created=datetime.now())
        letter.text = game.getMedia()
        letter.save()
        return letter


def create_tables():
    db.database.create_tables([User, Letter], safe=True)
    try:
        User.signup(name='test', email='test', password='test').save()
    except Exception, e:
        pass
