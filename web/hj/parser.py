import nltk
import re
import os
import treetaggerwrapper
import json
import cPickle

from gamedata import game

tagdir = os.path.join(os.path.dirname(__file__), "../../nlp/treetagger")
markovGrammar = cPickle.load(open(os.path.join(os.path.dirname(__file__), "../../nlp/markov_pairs_3.dat")))
#markovVoc = cPickle.load(open(os.path.join(os.path.dirname(__file__), "../../nlp/markov_pairs_voc_3.dat")))

treeTagger = treetaggerwrapper.TreeTagger(
    TAGLANG='nl',
    TAGDIR=tagdir,
    TAGPARFILE=tagdir + "/dutch-utf8.par")

alpinoTagger = nltk.data.load('taggers/alpino_brill_aubt.pickle')


def from_request(request):
    data = request.get_json()
    text = data['text'].lower() #re.sub("[^a-z]+", " ", data['text'].lower()).strip()
    print text

    words = nltk.word_tokenize(text)

    alpinoTags = alpinoTagger.tag(words)

    grammarScore = calculateGrammarScore(alpinoTags)
    vocScore = 0.0 #calculateVocScore(words)

    (html, cursor) = tag_text(data['text'], alpinoTags, data.get('cursor', 0))

    treeTags = treetaggerwrapper.make_tags(treeTagger.tag_text(text.lower()),
                                           exclude_nottags=True)
    lemmas = [t.lemma for t in treeTags]
    words = [t.word for t in treeTags]

    # word count
    word_count = len(re.sub("[^a-z]+", " ", data['text']).split(" "))

    matches, score, matching_lemmas = game.getTextMatches(lemmas)

    html = tag_html(text, words, lemmas, matching_lemmas)

    bs = calculate_bs(alpinoTags)

    acceptable = word_count > 10 and bs < 30
    
    return dict(
        text=data['text'],
        word_count=word_count,
        bs=bs,
        grammar_score=grammarScore,
        voc_score=vocScore,
        text_matches=matches,
        text_score=score,
        html=html,
        cursor=cursor,
        acceptable=acceptable)


def calculate_bs(alpinoTags):
    """
    Calculate the percentage of "bullshit" words (words
    unrecognized by the Alpino tagger)
    """
    if len(alpinoTags) == 0:
        return 0
    bstags = [t for t in alpinoTags if t[1] == '-None-']
    return int(100 * len(bstags)/float(len(alpinoTags)))


def tag_text(text, alpinoTags, cursorPos):
    tags = alpinoTags[:]
    startIndex = 0
    while len(tags) > 0:
        tag = tags[0]
        idx = text.lower().find(tag[0], startIndex)
        if idx > 0:
            l = len(tag[0])
            word = text[idx:idx+l]
            wrapPre = "<s class=\"%s\">" % tag[1]
            wrapPost = "</s>"
            wrapped = wrapPre + word + wrapPost
            text = text[:idx] + wrapped + text[idx+l:]
            if cursorPos < idx:
                pass
            elif cursorPos > idx + l:
                cursorPos += (len(wrapPre) + len(wrapPost))
            else:
                # cursor in middle of word
                cursorPos += len(wrapPre)
            startIndex = idx + len(wrapped)
        del tags[0]
    return (text, cursorPos)


def calculateGrammarScore(alpinoTags):
    tags = [t[1] for t in alpinoTags]
    score = 0.0
    for i in xrange(len(tags)-3):
        pair = tuple(tags[i:i+3])
        if pair in markovGrammar:
            score += markovGrammar[pair]
    d = float(len(tags)-3)
    if d == 0:
        score = 0
    else:
        score = score / d
    return min(100, int(100 * 100 * score))


def calculateVocScore(words):
    score = 0.0
    for i in xrange(len(words)-3):
        pair = tuple(words[i:i+3])
        if pair in markovVoc:
            score += markovVoc[pair]
    score = score / float(len(words)-3)
    return score


def index_sublist(lst, sublst):
    n = len(sublst)
    for i in xrange(len(lst)-n+1):
        if sublst == lst[i:i+n]:
            return i
    return -1


def tag_html(text, words, lemmas, matching_lemmas):
    ltext = text.lower()
    for m in matching_lemmas:
        lemma_idx = 0
        text_find_pos = 0
        while True:
            idx = index_sublist(lemmas[lemma_idx:], m)
            if idx >= 0:
                idx += lemma_idx
                #print "aa", m, idx
                collapsed = " ".join(words[idx:(idx+len(m))])
                pos = ltext.find(collapsed, text_find_pos)
                pos2 = pos + len(collapsed)
                text = text[:pos] + '<s>' + text[pos:pos2] + '</s>' + text[pos2:]
                ltext = text.lower()
                lemma_idx = idx+len(m)
                text_find_pos = pos2
            else:
                break
    return text

def tag_html_test(text, matching_lemmas):
    treeTags = treetaggerwrapper.make_tags(treeTagger.tag_text(text.lower()),
                                           exclude_nottags=False)
    lemmas = [t.lemma for t in treeTags]
    words = [t.word for t in treeTags]
    return tag_html(text, words, lemmas, matching_lemmas)
    
if __name__ == "__main__":
    t = u"""beste violet,

hoe gaat het nu? en met Meneer beer io?
ik ben benieuwd.
Jonathan is een oude bekende van me.
straffen is iets   fd jonathan zou me niet
"""
    print tag_html_test(t, [["meneer", "beren"], ["jonathan"], ["straf"]])
