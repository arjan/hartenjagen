from hj.app import app, mail
from flask_mail import Message

def main():
    msg = Message('Je hebt een brief gekregen', sender=app.config['DEFAULT_MAIL_SENDER'])
    msg.body = "Hoi %s,\n\nIk heb je een brief gestuurd! Ga snel naar de website om hem te lezen:\n%s\n\nGroetjes, Violet" % ("tester", app.config['BASE_URL'])
    msg.add_recipient('arjan@miraclethings.nl')
    mail.send(msg)
    print("Sent message.")

if __name__ == "__main__":
    app.config['DEBUG'] = True
    mail.init_app(app)
    with app.app_context():
        main()
