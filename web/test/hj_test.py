import json
import hj.main
import hj.models
from hj.app import mail
import unittest

import logging
logger = logging.getLogger('peewee')
#logger.setLevel(logging.DEBUG)
#logger.addHandler(logging.StreamHandler())


class HartenJagenTestCase(unittest.TestCase):

    def setUp(self):
        print "setUp {}".format(self)

        hj.main.app.config['TESTING'] = True
        mail.init_app(hj.main.app)
        
        hj.models.create_tables()
        self.app = hj.main.app.test_client()
        
    def tearDown(self):
        pass


# class StaticTestCase(HartenJagenTestCase):

#     def test_index(self):
#         rv = self.app.get('/')
#         assert 'ui-view="mainView"' in rv.data


class APITestCase(HartenJagenTestCase):

    def test_signup(self):
        rv = self.app.get('/api/signup')
        assert rv.status_code == 404

        # missing data
        rv = self.app.post('/api/signup')
        assert rv.status_code == 400

        rv = self.app.post('/api/signup', data=dict(email='foo', password='bar', name='foofoo'))
        assert rv.status_code == 200

        # duplicate user
        rv = self.app.post('/api/signup', data=dict(email='foo', password='bar', name='foofoo'))
        assert rv.status_code == 400

        # now try to login
        rv = self.app.post('/api/login')
        assert rv.status_code == 400

        # wrong user/pw
        rv = self.app.post('/api/login', data=dict(email='foo', password='barwrong'))
        assert rv.status_code == 401

        # OK
        rv = self.app.post('/api/login', data=dict(email='foo', password='bar'))
        assert rv.status_code == 200

        # now get profile;
        # unauth
        clean = hj.main.app.test_client()
        rv = clean.get('/api/profile')
        assert rv.status_code == 401

        # auth
        rv = self.app.get('/api/profile')
        assert rv.status_code == 200

        user = json.loads(rv.data)
        assert user['stats']['letter_count'] == 0
        assert user['stats']['word_count'] == 0

        # logout
        rv = self.app.post('/api/logout')
        assert rv.status_code == 200

        # unauth
        rv = self.app.get('/api/profile')
        assert rv.status_code == 401


    def test_parse(self):

        self.app.post('/api/login', data=dict(email='test', password='test'))

        d = self._draft('foo bar baz')
        assert d['word_count'] == 3
        assert d['acceptable'] == False

        d = self._draft('Dit is een eerste brief. Dit is gewoon wat tekst.')
        assert d['word_count'] == 12
        assert d['acceptable'] == True


    def test_flow(self):

        rv = self.app.post('/api/signup', data=dict(email='arjan+test1@miraclethings.nl', password='arjan', name='Arjan'))

        p = self._json_get('/api/profile')
        assert p['status'] == 'writing'
        assert p['score'] == 0
        
        # send first letter
        r = self._json_post('/api/send', {'text': 'Dit is een eerste brief. Dit is gewoon wat tekst.'})

        p = self._json_get('/api/profile')
        assert p['status'] == 'waiting'
        assert p['score'] == 0
        assert len(p['letters']) == 1

        # generate a letter
        r = self._json_post('/api/receive')

        # we are writing again
        p = self._json_get('/api/profile')
        assert p['status'] == 'writing'
        assert p['score'] == 0
        assert len(p['letters']) == 2

        # send another letter
        r = self._json_post('/api/send', {'text': 'Dit is een eerste brief. Jonathan meneer beer en zo dat zijn de triggerwoorden.'})
        p = self._json_get('/api/profile')
        print p
        assert p['status'] == 'waiting'
        assert p['score'] == 22
        assert len(p['letters']) == 3

        # generate a letter
        r = self._json_post('/api/receive')

        # we are writing again
        p = self._json_get('/api/profile')
        assert p['status'] == 'writing'
        assert p['score'] == 22
        assert len(p['letters']) == 4
        
        print p['letters']


    def _draft(self, text):
        return self._json_post('/api/draft', dict(text=text))

    def _profile(self):
        return self._json_get('/api/profile')
        
    def _json_post(self, path, data={}):
        rv = self.app.post(path, data=json.dumps(data), headers={'content-type': 'application/json'})
        assert rv.status_code == 200
        return json.loads(rv.data)

    def _json_get(self, path):
        rv = self.app.get(path)
        assert rv.status_code == 200
        return json.loads(rv.data)

        
if __name__ == '__main__':
    unittest.main()
